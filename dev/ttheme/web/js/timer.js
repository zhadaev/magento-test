define(['Magento_Customer/js/customer-data'], 
(customerData) => {
  "use strict";
  const  randomRGB = () => {
    var o = Math.round, r = Math.random, s = 255;
    return 'rgba(' + o(r()*s) + ',' + o(r()*s) + ',' + o(r()*s) + ')';
  }
  const color = randomRGB();
  customerData.set(['timerColor'], { color });
  const timer = document.querySelector('#timer');
  timer.style.color = customerData.get('timerColor')().color;
  let i = 0;
  setInterval(() => {
    timer.innerHTML = i++;
  }, 1000);
});